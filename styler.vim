" Styler -- make it easy to apply A-Style.
" Maintainer:   Igor S.K.
" Version:      0.0

let s:styler_version = "0.0"

if exists('g:loaded_styler') || &cp
  finish
endif
let g:loaded_styler = 1

" Location of the styler main tool
if !exists('g:styler_cmd')
    let g:styler_cmd = 'astyle'
endif

function s:StyleDiff()
    if &mod
        echo "File modified. Can not do styling."
        return ''
    endif

    let fname = expand('%:p')
    let ftype = &filetype
    let temp  = tempname()
    let filter_cmd = 'cat '.fname.' | '.g:styler_cmd

    execute 'rightbelow vertical split '.temp
    execute '0read !'.filter_cmd
    let &filetype = ftype
    let &mod = !&mod
    diffthis
    wincmd p
    diffthis
    wincmd p
    return ''
endfunction

function s:StyleRun()
    if &mod
        echo "File modified. Can not do styling."
        return ''
    endif

    execute '%!'.g:styler_cmd
endfunction


exe 'command Sdiff call s:StyleDiff()'
exe 'command Srun call s:StyleRun()'
